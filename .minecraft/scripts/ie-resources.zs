mods.immersiveengineering.Excavator.addMineral("Alkali and Alkaline metals", 19, 0.1, ["oreTin", "oreLithium", "oreMagnesium"], [0.55, 0.30, 0.15 ]);
mods.immersiveengineering.Excavator.addMineral("Thorium ore", 21, 0.1, ["oreThorium", "oreBoron"], [0.70, 0.30]);
mods.immersiveengineering.Excavator.addMineral("Inginous rocks ore", 30, 0.1, ["stoneGranite", "stoneAndersite", "stoneDiorite", "stoneMarble", "obsidian"], [0.22, 0.22, 0.22, 0.22, 0.12 ]);
mods.immersiveengineering.Excavator.addMineral("Zinc ore", 23, 0.1, ["oreZinc", "oreLead", "oreMagnesium"], [0.71, 0.09, 0.20]);
mods.immersiveengineering.Excavator.addMineral("Hellish ore", 18, 0.2, ["oreCobalt", "oreArdite", "oreAncientDebris"], [0.55, 0.44, 0.01], [-1]);
