import crafttweaker.item.IItemStack;
import crafttweaker.liquid.ILiquidStack;
import crafttweaker.oredict.IOreDictEntry;



val oreMelterLiquids = [ <liquid:iron>, <liquid:copper>, <liquid:silver>, <liquid:lead>, <liquid:gold>, <liquid:tin>, <liquid:aluminum>, <liquid:uranium> ] as ILiquidStack[];
val oreMelterOres    = [ <ore:oreIron>, <ore:oreCopper>, <ore:oreSilver>, <ore:oreLead>, <ore:oreGold>, <ore:oreTin>, <ore:oreAluminum>, <ore:oreUranium> ] as IOreDictEntry[];

for index, oreGroup in oreMelterOres {
  for item in oreGroup.items {
    mods.tcomplement.Overrides.removeRecipe(oreMelterLiquids[index],item);
    mods.tcomplement.Overrides.addRecipe(oreMelterLiquids[index],item,513); // in Kelvin (in mincraft K=C+300)
  }
}

//recipes.remove(<tcomplement:porcelain_melter>);
//furnace.remove(<tconstruct:materials>);
//recipes.remove(<tcomplement:porcelain_casting:1>);
//recipes.remove(<tcomplement:porcelain_tank:2>);
//recipes.remove(<tcomplement:porcelain_tank:1>);
//recipes.remove(<tcomplement:porcelain_tank>);


//recipes.addShaped(<tcomplement:porcelain_melter>, [[<forestry:loam>, <ceramics:unfired_clay:5>, <forestry:loam>],[<ceramics:unfired_clay:5>, <forestry:loam>, <ceramics:unfired_clay:5>], [<ceramics:unfired_clay:5>, <ceramics:unfired_clay:5>, <ceramics:unfired_clay:5>]]);


mods.tconstruct.Casting.addTableRecipe(<tconstruct:cast_custom>, <minecraft:brick>, <liquid:copper>, 576, true, 300);


//mods.tconstruct.Melting.addRecipe(<liquid:iron> * 144,<minecraft:iron_ingot>, 500);
//plates casting recipes x2 ingots(288)
//mods.tconstruct.Casting.addTableRecipe(IItemStack output, IItemStack cast, ILiquidStack fluid, int amount, @Optional boolean consumeCast, @Optional int time);
//copper plate
mods.tconstruct.Casting.removeTableRecipe(<immersiveengineering:metal:30>);
mods.tconstruct.Casting.addTableRecipe(<immersiveengineering:metal:30>, <tconstruct:cast_custom:3>, <liquid:copper>, 288, false, 200);
//iron plate
mods.tconstruct.Casting.removeTableRecipe(<immersiveengineering:metal:39>);
mods.tconstruct.Casting.addTableRecipe(<immersiveengineering:metal:39>, <tconstruct:cast_custom:3>, <liquid:iron>, 288, false, 200);
//steelplate 
mods.tconstruct.Casting.removeTableRecipe(<immersiveengineering:metal:38>);
mods.tconstruct.Casting.addTableRecipe(<immersiveengineering:metal:38>, <tconstruct:cast_custom:3>, <liquid:steel>, 288, false, 200);
//golden plate
mods.tconstruct.Casting.removeTableRecipe(<immersiveengineering:metal:40>);
mods.tconstruct.Casting.addTableRecipe(<immersiveengineering:metal:40>, <tconstruct:cast_custom:3>, <liquid:gold>, 288, false, 200);
//electrum plate
mods.tconstruct.Casting.removeTableRecipe(<immersiveengineering:metal:37>);
mods.tconstruct.Casting.addTableRecipe(<immersiveengineering:metal:37>, <tconstruct:cast_custom:3>, <liquid:electrum>, 288, false, 200);
//alumin plate
mods.tconstruct.Casting.removeTableRecipe(<immersiveengineering:metal:31>);
mods.tconstruct.Casting.addTableRecipe(<immersiveengineering:metal:31>, <tconstruct:cast_custom:3>, <liquid:aluminum>, 288, false, 200);
//lead plate
mods.tconstruct.Casting.removeTableRecipe(<immersiveengineering:metal:32>);
mods.tconstruct.Casting.addTableRecipe(<immersiveengineering:metal:32>, <tconstruct:cast_custom:3>, <liquid:lead>, 288, false, 200);
//silver plate
mods.tconstruct.Casting.removeTableRecipe(<immersiveengineering:metal:33>);
mods.tconstruct.Casting.addTableRecipe(<immersiveengineering:metal:33>, <tconstruct:cast_custom:3>, <liquid:silver>, 288, false, 200);
//nickel plate
mods.tconstruct.Casting.removeTableRecipe(<immersiveengineering:metal:34>);
mods.tconstruct.Casting.addTableRecipe(<immersiveengineering:metal:34>, <tconstruct:cast_custom:3>, <liquid:nickel>, 288, false, 200);
//uranium plate
mods.tconstruct.Casting.removeTableRecipe(<immersiveengineering:metal:35>);
mods.tconstruct.Casting.addTableRecipe(<immersiveengineering:metal:35>, <tconstruct:cast_custom:3>, <liquid:uranium>, 288, false, 200);
//constantan plate
mods.tconstruct.Casting.removeTableRecipe(<immersiveengineering:metal:36>);
mods.tconstruct.Casting.addTableRecipe(<immersiveengineering:metal:36>, <tconstruct:cast_custom:3>, <liquid:constantan>, 288, false, 200);


//seared bricks removal
mods.tconstruct.Casting.removeTableRecipe(<tconstruct:materials>);
mods.tconstruct.Melting.removeRecipe(<liquid:stone>);


//remove some alloying
mods.tconstruct.Alloy.removeRecipe(<liquid:knightslime>);
mods.tconstruct.Alloy.removeRecipe(<liquid:manyullyn>);
//mods.tconstruct.Alloy.removeRecipe(<liquid:pigiron>);

//tcomplement overrides
mods.tcomplement.Blacklist.addRecipe(<liquid:steel>, <immersiveengineering:metal:8>);
mods.tcomplement.Blacklist.addRecipe(<liquid:steel>, <immersiveengineering:metal:28>);
mods.tcomplement.Blacklist.addRecipe(<liquid:steel>, <immersiveengineering:metal:17>);
mods.tcomplement.Blacklist.addRecipe(<liquid:iron>, <minecraft:iron_bars>);
mods.tcomplement.Blacklist.addRecipe(<liquid:iron>, <immersiveengineering:metal:29>);
mods.tcomplement.Blacklist.addRecipe(<liquid:aluminum>, <immersiveengineering:ore:1>);
mods.tcomplement.Blacklist.addRecipe(<liquid:uranium>, <immersiveengineering:ore:5>);

mods.tcomplement.Blacklist.addRecipe(<liquid:stone>, <tconstruct:soil>);

//iron
mods.tcomplement.Blacklist.addRecipe(<liquid:iron>, <minecraft:iron_nugget>);
//mods.tcomplement.Blacklist.addRecipe(<liquid:iron>, <ore:dustIron>);
mods.tcomplement.Blacklist.addRecipe(<liquid:iron>, <minecraft:iron_ingot>);
//mods.tcomplement.Blacklist.addRecipe(<liquid:iron>, <minecraft:iron_ore>);

//mods.tcomplement.Blacklist.removeRecipe(<liquid:copper>);
//mods.tcomplement.Blacklist.removeRecipe(<liquid:tin> * 16, <ore:oreTin>);
//mods.tcomplement.Blacklist.removeRecipe(<liquid:lead> * 16, <ore:oreLead>);
//mods.tcomplement.Blacklist.removeRecipe(<liquid:silver> * 16, <ore:oreSilver>);

//mods.tcomplement.Overrides.addRecipe(<liquid:copper> * 16, <immersiveengineering:ore>, 50);

//mods.tcomplement.Overrides.addRecipe(<liquid:bronze> * 144, <forestry:ingot_bronze>, 200);
